﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FSTECParser
{
    public partial class Form3 : Form
    {
        
        public Form3(Threat threat,Threat threatOld)
        {
            InitializeComponent();
            //отображение актуально информации по выделенной строчке
            listView2.Items.Add(new ListViewItem(new string[] { "Идентификатор", "УБИ." + threat.Id.ToString() }));
            listView2.Items.Add(new ListViewItem(new string[] { "Наименование", threat.ThreatName }));
            listView2.Items.Add(new ListViewItem(new string[] { "Описание", threat.ThreatDescription }));
            listView2.Items.Add(new ListViewItem(new string[] { "Источник угрозы", threat.ThreatSource }));
            listView2.Items.Add(new ListViewItem(new string[] { "Объект воздействия", threat.ThreatObject }));
            listView2.Items.Add(new ListViewItem(new string[] { "Нарушение конфиденциальности", threat.Сonfidentiality ? "Да" : "Нет" }));
            listView2.Items.Add(new ListViewItem(new string[] { "Нарушение целостности", threat.Integrity ? "Да" : "Нет" }));
            listView2.Items.Add(new ListViewItem(new string[] { "Нарушение доступности", threat.Availability ? "Да" : "Нет" }));

            //отображение старой инфомации по выделенной информации
            if (threatOld!=null)
            {
                listView1.Items.Add(new ListViewItem(new string[] { "Идентификатор", "УБИ." + threatOld.Id.ToString() }));
                listView1.Items.Add(new ListViewItem(new string[] { "Наименование", threatOld.ThreatName }));
                listView1.Items.Add(new ListViewItem(new string[] { "Описание", threatOld.ThreatDescription }));
                listView1.Items.Add(new ListViewItem(new string[] { "Источник угрозы", threatOld.ThreatSource }));
                listView1.Items.Add(new ListViewItem(new string[] { "Объект воздействия", threatOld.ThreatObject }));
                listView1.Items.Add(new ListViewItem(new string[] { "Нарушение конфиденциальности", threatOld.Сonfidentiality ? "Да" : "Нет" }));
                listView1.Items.Add(new ListViewItem(new string[] { "Нарушение целостности", threatOld.Integrity ? "Да" : "Нет" }));
                listView1.Items.Add(new ListViewItem(new string[] { "Нарушение доступности", threatOld.Availability ? "Да" : "Нет" }));
            }
            else
            {
                string[] namesItem = new string[] { "Идентификатор", "Наименование", "Описание", "Источник угрозы", "Объект воздействия", "Нарушение конфиденциальности", "Нарушение целостности", "Нарушение доступности" };
                for (int i=0;i<namesItem.Length;i++)
                {
                    listView1.Items.Add(new ListViewItem(new string[] { namesItem[i], "-" }));
                }
            }
            
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
