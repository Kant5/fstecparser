﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FSTECParser
{
    public partial class Form2 : Form
    {  
        public Form2(Threat threat)
        {
            InitializeComponent();
            //отображение информации по выделенной строке
            listView1.Items.Add(new ListViewItem(new string[] { "Идентификатор", "УБИ." + threat.Id.ToString() }));
            listView1.Items.Add(new ListViewItem(new string[] { "Наименование", threat.ThreatName }));
            listView1.Items.Add(new ListViewItem(new string[] { "Описание", threat.ThreatDescription }));
            listView1.Items.Add(new ListViewItem(new string[] { "Источник угрозы", threat.ThreatSource }));
            listView1.Items.Add(new ListViewItem(new string[] { "Объект воздействия", threat.ThreatObject }));
            listView1.Items.Add(new ListViewItem(new string[] { "Нарушение конфиденциальности", threat.Сonfidentiality ? "Да" : "Нет" }));
            listView1.Items.Add(new ListViewItem(new string[] { "Нарушение целостности", threat.Integrity ? "Да" : "Нет" }));
            listView1.Items.Add(new ListViewItem(new string[] { "Нарушение доступности", threat.Availability ? "Да" : "Нет" }));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
