﻿using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Numeric;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace FSTECParser
{
    public partial class Form1 : Form
    {
        public static List<Threat> threatList = new List<Threat>();//локальная бызы
        public static List<Threat> threatListOld = new List<Threat>();//старая локальная база
        private int numberList = 1;
        private int countList;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                //скачивание информации с сайта в файл
                new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx",
                    "D:\\IO\\ThreatListFSTEC.xlsx");

                //обновления и показ 
                if (File.Exists("D:\\IO\\ThreatListFSTEC.xlsx"))
                {
                    threatListOld.Clear();
                    threatListOld = threatList.GetRange(0, threatList.Count);
                    ThreatParse("D:\\IO\\ThreatListFSTEC.xlsx");
                    countList = (threatList.Count / 15) + (threatList.Count % 15 == 0 ? 0 : 1);
                    numberList = 1;
                    listView1.Items.Clear();
                    button2.Enabled = true;
                    List<Threat> list = new List<Threat>();
                    list = threatList.GetRange(0, 15);
                    for(int i =0;i<15;i++)
                    {
                        listView1.Items.Add(DoItem(list.ElementAt(i)));
                    }
                    label1.Text = "Страница: " + numberList + "  из " + countList;
                    MessageBox.Show("Статуса обновления: Успешно\r\nОбщего количества обновленных записе: " + threatList.Count, "Обновление данных", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Статуса обновления: Ошибка\r\nОтсутствует файл с данными!", "Ошибка изъятия данных из файла", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Статуса обновления: Ошибка\r\n" + ex.Message, "Ошибка закгрузки", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Метод DoItem() создает строку
        /// для ListView из строк
        /// в локальной базе
        /// </summary>
        /// <param name="threat">Строка из локальной базы</param>
        /// <returns>Строка для ListView</returns>
        private ListViewItem DoItem(Threat threat)
        {
            ListViewItem item;
            item = new ListViewItem(new string[] { "УБИ." + threat.Id.ToString(), threat.ThreatName });
            if (threatListOld.Count != 0)
            {
                 int index = threatListOld.FindIndex(delegate (Threat t) { return threat.Id == t.Id; });
                 if (index == -1) item.BackColor = Color.Purple;
                 else
                 {
                     if (!threat.Equals(threatListOld.ElementAt(index))) item.BackColor = Color.Purple;
                 }
            }
            return item;
        }
        /// <summary>
        /// Метод ThreatParse() извлекает из
        /// файла информацию и записывает ее в локальную базу
        /// </summary>
        /// <param name="fileName">Полный путь к файлу</param>
        private void ThreatParse(string fileName)
        { 
            using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(fileName)))
            {
                threatList.Clear();
                var myWorksheet = xlPackage.Workbook.Worksheets.First();
                var totalRows = myWorksheet.Dimension.End.Row;
                var totalColumns = myWorksheet.Dimension.End.Column;

                for (int rowNum = 3; rowNum <= totalRows; rowNum++)
                {
                    var row = myWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                    string[] coloms = string.Join("||", row).Split(new char[] { '|','|'}, StringSplitOptions.RemoveEmptyEntries);
                    try
                    {
                        Threat threat = MakeThreat(coloms);
                        threatList.Add(threat);
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
        }
        /// <summary>
        /// Метод ConvertValue() конвертирует
        /// 0 и 1 в bool значение
        /// </summary>
        /// <param name="value">Строка со значением</param>
        private bool ConvertValue(string value)
        {
            return value.Equals("1");
        }
        /// <summary>
        /// Метод MakeThreat() заполняет
        /// новую строку для локальной базы
        /// </summary>
        /// <param name="value">Строка со значением</param>
        private Threat MakeThreat(string[] coloms)
        {
            return new Threat(Convert.ToInt32(coloms[0]), coloms[1], coloms[2], coloms[3], coloms[4], ConvertValue(coloms[5]), ConvertValue(coloms[6]), ConvertValue(coloms[7]));
        }


        private void Form1_Shown(object sender, EventArgs e)
        {
            if (!File.Exists("D:\\IO\\ThreatListFSTEC.xlsx"))
            {
                if (MessageBox.Show("Файл с данными не скачен.\r\nСкачать файл?", "Нет файла", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx",
                       "D:\\IO\\ThreatListFSTEC.xlsx");
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка закгрузки", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                }
                else
                {
                    this.Close();
                }
            }
            ThreatParse("D:\\IO\\ThreatListFSTEC.xlsx");
            countList = (threatList.Count / 15) + (threatList.Count % 15 == 0 ? 0 : 1);
            numberList = 1;
            listView1.Items.Clear();
            button2.Enabled = true;
            List<Threat> list = new List<Threat>();
            list = threatList.GetRange(0, 15);
            foreach (var it in list)
            {
                ListViewItem item = new ListViewItem(new string[] { "УБИ." + it.Id.ToString(), it.ThreatName });
                listView1.Items.Add(item);
            }
            label1.Text = "Страница: " + numberList + "  из " + countList;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            numberList--;
            listView1.Items.Clear();
            if (numberList < countList) button2.Enabled = true;
            List<Threat> list = new List<Threat>();
            if (numberList == 1)
            {
                list = threatList.GetRange(0, 15);
                button1.Enabled = false;
            }
            else
            {
                list = threatList.GetRange(15 * (numberList - 1), 15);
            }
            for(int i=0;i<list.Count;i++)
            {
                listView1.Items.Add(DoItem(list.ElementAt(i)));
            }
            label1.Text = "Страница: " + numberList + "  из " + countList;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            numberList++;
            listView1.Items.Clear();
            if (numberList > 1) button1.Enabled = true;
            List<Threat> list = new List<Threat>();
            if (numberList == countList)
            {
                list = threatList.GetRange(15 * (numberList - 1), threatList.Count- 15 * (numberList - 1));
                button2.Enabled = false;
            }
            else
            {
                list = threatList.GetRange(15 * (numberList-1), 15);
            }
            for(int i=0;i<list.Count;i++)
            {
                listView1.Items.Add(DoItem(list.ElementAt(i)));
            }
            label1.Text = "Страница: " + numberList + "  из " + countList;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //сохранение базы в файл
            using (FileStream fs = new FileStream("D:\\IO\\DataThreatFSTEC.xlsx", FileMode.Create))
            {
                using (ExcelPackage xlPackage = new ExcelPackage(fs))
                {
                    //добавления нового листа в exls файл
                    xlPackage.Workbook.Worksheets.Add("Угрозы");
                    List<string[]> data = new List<string[]>();
                    //добавление заголовков
                    data.Add(new string[] { "Идентификатор УБИ", "Наименование УБИ", "Описание", "Источник угрозы (характеристика и потенциал нарушителя)", "Объект воздействия", "Нарушение конфиденциальности", "Нарушение целостности", "Нарушение доступности" });
                    foreach (var it in threatList)
                    {
                        data.Add(new string[] { it.Id.ToString(), it.ThreatName, it.ThreatDescription, it.ThreatSource, it.ThreatObject, it.Сonfidentiality.ToString(), it.Integrity.ToString(), it.Availability.ToString() });
                    }
                    string headerRange = "A1: " + Char.ConvertFromUtf32(data[0].Length + 64) + "1";
                    var myWorksheet = xlPackage.Workbook.Worksheets["Угрозы"];
                    myWorksheet.Cells[headerRange].LoadFromArrays(data);
                    //изменение ширины полей
                    myWorksheet.Column(1).Width = 10;
                    myWorksheet.Column(2).Width = 70;
                    myWorksheet.Column(3).Width = 70;
                    myWorksheet.Column(4).Width = 70;
                    myWorksheet.Column(5).Width = 70;
                    myWorksheet.Column(6).Width = 20;
                    myWorksheet.Column(7).Width = 20;
                    myWorksheet.Column(8).Width = 20;
                    xlPackage.SaveAs(fs);
                }
            }
        }
  
        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
                return;
            //получание выделенной строки
            ListViewItem item = listView1.SelectedItems[0];
            string d = listView1.SelectedItems[0].Text;
            int id = Convert.ToInt32(Regex.Replace(d, @"[^\d]+", ""));
            Threat t = threatList.Find((x) => x.Id == id);
            //открытие форм с описание выделенной строки
            if(item.BackColor==Color.Purple)
            {
                Threat tOld = threatListOld.Find((x) => x.Id == id);
                Form form3 = new Form3(t, tOld);//форма если строка была изменена
                form3.ShowDialog(this);
            }
            else
            {
                Form form2 = new Form2(t);//форма если строка не была изменена
                form2.ShowDialog(this);
            }
        }
    }
}
